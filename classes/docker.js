const Dockerorde = require("dockerode");
const DEFAULT_SOCKET_PATH = "/var/run/docker.sock";
const {
  printedFieldsInHealthCheck,
  printContainers,
  sleep,
  containerFormat,
  keepItems,
  keepFields
} = require("./helper");

class Docker {
  constructor() {
    const { DOCKER_HOST, DOCKER_PORT, DOCKER_SOCKET_PATH } = process.env;
    const dockerConfig = {};

    if (DOCKER_HOST && DOCKER_PORT) {
      dockerConfig.host = DOCKER_HOST;
      dockerConfig.port = DOCKER_PORT;
    } else {
      dockerConfig.socketPath = DOCKER_SOCKET_PATH || DEFAULT_SOCKET_PATH;
    }
    this.docker = new Dockerorde(dockerConfig);
  }

  async listContainers({ fields, filters, all } = {}) {
    const _containers = await this.docker.listContainers({ all: !!all });
    let containers = _containers.map(containerFormat);

    if (filters) {
      containers = containers.filter(keepItems(filters));
    }

    if (fields) {
      containers = containers.map(keepFields(fields));
    }
    return containers;
  }

  async listContainersRaw() {
    const _containers = await this.docker.listContainers();
    return _containers;
  }

  async waitForHealthyServicesReach(
    project,
    service,
    count,
    interval = 10,
    timeout = 10,
    quiet = false
  ) {
    const t0 = Date.now();
    let tried = 0,
      healthyCount = 0;
    const options = {
      filters: {
        Project: project,
        Service: service,
        Healthy: true,
      },
      fields: printedFieldsInHealthCheck,
    };

    while (tried < interval) {
      const containers = await this.listContainers(options);
      healthyCount = containers.length;
      if (!quiet) {
        printInfo(
          containers,
          project,
          service,
          timeout,
          count,
          interval,
          tried,
          healthyCount
        );
      }
      if (healthyCount === count) {
        break;
      }
      await sleep(timeout * 1000);
      tried++;
    }

    if (healthyCount === count) {
      const containers = await this.listContainers(options);
      const elapsed = Math.floor((Date.now() - t0) / 1000);
      printInfo(
        containers,
        project,
        service,
        timeout,
        count,
        interval,
        tried,
        healthyCount,
        elapsed
      );
      return;
    }

    throw new Error("Timeout");
  }
}

function printInfo(
  containers,
  project,
  service,
  timeout,
  count,
  interval,
  tried,
  healthyCount,
  elapsed
) {
  try {
    const time = new Date().toLocaleTimeString();
    console.clear();
    console.log(
      `Wait for service "${project}/${service}" reach ${count} healthy instance(s)...`
    );
    if (elapsed) {
      console.log(`.. Success in ${elapsed}s`);
    } else {
      console.log(
        `.. ${time}: retry ${tried}/${interval} - sleep ${timeout} second(s) - healthy: ${healthyCount}/${count}`
      );
    }
    printContainers(containers);
  } catch (e) {
    console.log("printInfo failed");
    console.error(e);
  }
}

module.exports = Docker;
