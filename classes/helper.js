const {printTableFromObjects} = require('../libs/js-print-table');

const printedFieldsInHealthCheck = (exports.printedFieldsInHealthCheck = [
  "Name",
  "Image",
  "IP",
  "Created",
  "Healthy",
]);

exports.keepFields = function keepFields(item, props) {
  function fn(fields) {
    return function (item) {
      return fields.reduce((out, key) => {
        return Object.assign(out, { [key]: item[key] });
      }, {});
    };
  }

  if (props) {
    return Array.isArray(item) ? item.map(fn(props)) : fn(props)(item);
  }

  const _props = item;
  return fn(_props);
};

exports.keepItems = function keepItems(items, props) {
  function fn(fields) {
    return function checkItemHasMatched(item) {
      for (let key of Object.keys(fields)) {
        const values = Array.isArray(fields[key]) ? fields[key] : [fields[key]];
        if (!values.some(v => item[key] == v)) return false;
      }
      return true;
    };
  }

  // return the filtered array
  if (props) {
    return items.filter(fn(props));
  }

  // 1 parameter -> return closure function for arr.filter(fn)
  const _props = items;
  return fn(_props);
};

exports.printContainers = function printContainers(_containers) {
  const containers = _containers.map((c) => {
    const created = new Date(c.Created * 1000);
    const now = new Date();
    const isSameDate = created.getDate() == now.getDate();

    // keep 2 last words
    c.Image = c.Image.split("/").slice(-2).join("/");
    c.Created = isSameDate
      ? created.toLocaleTimeString()
      : created.toLocaleDateString();
    return c;
  });
  printTableFromObjects(containers, {
    // rowBorders: true,
  });
};

exports.sleep = function sleep(ms) {
  return new Promise((r) => {
    setTimeout(() => {
      r(true);
    }, ms);
  });
};

exports.containerFormat = function containerFormat(containerInfo) {
  const { Id, Image, Ports, Created, Labels, State, Status } = containerInfo;

  const Names = containerInfo.Names.map((name) => name.replace(/_/g, "-")).map(
    (name) => name.replace(/^\//g, "")
  );

  const Service = Labels["com.docker.compose.service"] || "";
  const Project = Labels["com.docker.compose.project"] || "";
  const WorkingDir = Labels["com.docker.compose.working_dir"];
  const Networks = containerInfo.NetworkSettings.Networks;
  const networkName = Object.keys(Networks)[0];
  const net = Networks[networkName];
  const IP = net.IPAddress;
  const Name =
    Names.find((name) => name.includes(`${Project}-${Service}`)) || Names[0];
  const ServiceNo = parseInt(Name.split("-").pop());

  const healthyText = `${Status} | ${State}`.toLowerCase();
  const Healthy = healthyText.includes("(healthy)")
    ? true
    : healthyText.includes("(unhealthy)")
    ? false
    : null;

  return {
    Id,
    Names,
    Image,
    Name,
    IP,
    Ports,
    Created,
    Project,
    Service,
    ServiceNo,
    WorkingDir,
    State,
    Status,
    Healthy,
  };
};

exports.isMatched = function isMatched(object, key, values) {
  const filterValues = Array.isArray(values) ? values : [values];
  const itemValue = object[key];
  const isOk = filterValues.some((filterValue) => {
    return filterValue == itemValue;
  });
  return isOk;
};

function isNumber(a) {
  if (typeof a === 'number') return true;
  if (typeof a !== 'string') return false;
  // has character not in {0-9.,}
  if (a.match(/[^\d.,]/g)) return false;
}


function wrapValue(s) {
  if (typeof s !== 'string') return s;
  if (s.includes(`"`)) return `'${s}'`;
  return `"${s}"`
}

exports.isNumber = isNumber;
exports.wrapValue = wrapValue;