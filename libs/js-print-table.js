/*
 * credits: https://github.com/panzi/js-print-table
 */ 

const AlignRegExp = /^[- .<>]*$/;
const JsonLexRegExp = /("(?:\\["\\]|[^"])*"|'(?:\\['\\]|[^'])*')|(\btrue\b|\bfalse\b)|(\bnull\b|\bundefined\b)|(\b\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d+)?(?:Z|[-+]?\d{2}:?\d{2})?\b|\b-?[0-9]+(?:n\b|(?:\.[0-9]*)?(?:[eE][-+]?[0-9]+)?)|-?\bInfinity\b|\bNaN\b)|(\[Function[^\]]*\])|(<ref[^>]*>|\[(?:Function|Circular|Array|Object)[^\]]*\])|(Symbol\([^)]*\))/g;
const JSEscapeRegExp = /\\(?:["\\/bfnrt]|u[0-9a-fA-F]{4}|x[0-9a-fA-F]{2})/g;
const WeightBold = '\u001b[1m';
const ResetFormat = '\u001b[0m';
// const ColorBlack   = '\u001b[30m';
const ColorRed = '\u001b[31m';
const ColorGreen = '\u001b[32m';
const ColorYellow = '\u001b[33m';
const ColorBlue = '\u001b[34m';
const ColorMagenta = '\u001b[35m';
const ColorCyan = '\u001b[36m';
// const ColorWhite   = '\u001b[37m';
// const ColorBrightBlack   = '\u001b[30;1m';
// const ColorBrightRed     = '\u001b[31;1m';
// const ColorBrightGreen   = '\u001b[32;1m';
// const ColorBrightYellow  = '\u001b[33;1m';
// const ColorBrightBlue    = '\u001b[34;1m';
// const ColorBrightMagenta = '\u001b[35;1m';
// const ColorBrightCyan    = '\u001b[36;1m';
// const ColorBrightWhite   = '\u001b[37;1m';
/**
 * ANSI escape sequences for colors.
 */
const AnsiColors = exports.AnsiColors = {
    number: ColorGreen,
    null: ColorBlue,
    string: ColorMagenta,
    symbol: ColorRed,
    function: ColorCyan,
    other: ColorCyan,
    escape: ColorYellow,
    boolean: ColorGreen,
    header: WeightBold,
    reset: ResetFormat,
};
/**
 * No color codes at all, just empty strings.
 */
const NoColors = exports.NoColors = {
    number: '',
    null: '',
    string: '',
    symbol: '',
    function: '',
    other: '',
    escape: '',
    boolean: '',
    header: '',
    reset: '',
};
// \u{FE00}-\u{FE0F} variant selectors
// \u{200B}-\u{200D} zero width space, zero-width non-joiner, zero-width joiner
// \u{2060} word joiner
// \u{FEFF} zero-width no-break space
const IgnoreRegExp = /\p{Nonspacing_Mark}|\p{Default_Ignorable_Code_Point}|[\u{FE00}-\u{FE0F}\u{200B}-\u{200D}\u{2060}\u{FEFF}]/gu;
// \u{3040}-\u{A4CF} I don't really know. Guessed Asian scripts
// \u{AC00}-\u{D7FF} Korean Hangul
// \u{20000}-\u{323AF} CJK Unified Ideographs Extensions
// \u{FF01}-\u{FF60} Asian full-width characters
// \u{FFE0}-\u{FFE6} Asian full-width characters
const DoubleWidthRegExp = /\p{Emoji_Presentation}|[\u{3040}-\u{A4CF}\u{AC00}-\u{D7FF}\u{20000}-\u{323AF}\u{FF01}-\u{FF60}\u{FFE0}-\u{FFE6}\0\r\v\f]/gu;
const SurrogatePairRegExp = /[\u{D800}-\u{10FFFF}]/gu;
const ControlRegExp = /\p{Control}/gu;
const CharReplacement = {
    '\0': '\\0',
    '\r': '\\r',
    '\v': '\\v',
    '\f': '\\f', // '^L',
};
const SpecialCharRegExp = /([\0\r\v\f])|([\u{0}-\u{9}\u{B}-\u{1F}\u{7F}-\u{9F}])/gu;
// TODO: match more ANSI escape sequences
const AnsiEscapeRegExp = /\u001b\[\d+(?:;\d+)*m/gu;
/**
 * TableStyle that is used per default. Any supplied table style can be partial
 * and overrides the properties of this style.
 *
 * ```text
 * rowBorder: false
 * columnBorder: false
 * ┌──────────────┐
 * │ Hdr 1  Hdr 2 │
 * ├──────────────┤
 * │ foo        0 │
 * │ bar       12 │
 * │ baz      345 │
 * └──────────────┘
 *
 * rowBorder: true
 * columnBorder: false
 * ┌──────────────┐
 * │ Hdr 1  Hdr 2 │
 * ╞══════════════╡
 * │ foo        0 │
 * ├──────────────┤
 * │ bar       12 │
 * ├──────────────┤
 * │ baz      345 │
 * └──────────────┘
 *
 * rowBorder: false
 * columnBorder: true
 * ┌───────┬───────┐
 * │ Hdr 1 │ Hdr 2 │
 * ├───────┼───────┤
 * │ foo   │     0 │
 * │ bar   │    12 │
 * │ baz   │   345 │
 * └───────┴───────┘
 *
 * rowBorder: true
 * columnBorder: true
 * ┌───────┬───────┐
 * │ Hdr 1 │ Hdr 2 │
 * ╞═══════╪═══════╡
 * │ foo   │     0 │
 * ├───────┼───────┤
 * │ bar   │    12 │
 * ├───────┼───────┤
 * │ baz   │   345 │
 * └───────┴───────┘
 * ```
 */
const DefaultTableStyle = exports.DefaultTableStyle = {
    columnBorder: ' │ ',
    columnNoBorder: '  ',
    paddingChar: ' ',
    leftOutline: '│ ',
    rightOutline: ' │',
    topOutlineColumnBorder: '─┬─',
    borderCrossing: '─┼─',
    noBorderCrossing: '──',
    bottomOutlineColumnBorder: '─┴─',
    topLeftOutline: '┌─',
    bottomLeftOutline: '└─',
    topRightOutline: '─┐',
    bottomRightOutline: '─┘',
    rowBorder: '─',
    horizontalOutline: '─',
    leftOutlineRowBorder: '├─',
    rightOutlineRowBorder: '─┤',
    leftHeaderRowBorder: '╞═',
    rightHeaderRowBorder: '═╡',
    headerBorder: '═',
    headerBorderCrossing: '═╪═',
    headerNoBorderCrossing: '══',
    outlineNoBorderCrossing: '──',
};
/**
 * All borders and outlines are just whitespace.
 *
 * ```text
 * rowBorder: false
 * columnBorder: false
 *
 *   Hdr 1  Hdr 2
 *
 *   foo        0
 *   bar       12
 *   baz      345
 *
 *
 * rowBorder: true
 * columnBorder: false
 *
 *   Hdr 1  Hdr 2
 *
 *   foo        0
 *
 *   bar       12
 *
 *   baz      345
 *
 *
 * rowBorder: false
 * columnBorder: true
 *
 *   Hdr 1  Hdr 2
 *
 *   foo        0
 *   bar       12
 *   baz      345
 *
 *
 * rowBorder: true
 * columnBorder: true
 *
 *   Hdr 1  Hdr 2
 *
 *   foo        0
 *
 *   bar       12
 *
 *   baz      345
 *
 * ```
 */
const SpaceTableStyle = exports.SpaceTableStyle = {
    columnBorder: '  ',
    columnNoBorder: '  ',
    paddingChar: ' ',
    leftOutline: ' ',
    rightOutline: ' ',
    topOutlineColumnBorder: '  ',
    borderCrossing: '  ',
    noBorderCrossing: '  ',
    bottomOutlineColumnBorder: '  ',
    topLeftOutline: ' ',
    bottomLeftOutline: ' ',
    topRightOutline: ' ',
    bottomRightOutline: ' ',
    rowBorder: ' ',
    horizontalOutline: ' ',
    leftOutlineRowBorder: ' ',
    rightOutlineRowBorder: ' ',
    leftHeaderRowBorder: ' ',
    rightHeaderRowBorder: ' ',
    headerBorder: ' ',
    headerBorderCrossing: '  ',
    headerNoBorderCrossing: '  ',
    outlineNoBorderCrossing: '  ',
};
/**
 * Only use ASCII characters for the table outline and borders.
 *
 * ```text
 * rowBorder: false
 * columnBorder: false
 * +--------------+
 * | Hdr 1  Hdr 2 |
 * +--------------+
 * | foo        0 |
 * | bar       12 |
 * | baz      345 |
 * +--------------+
 *
 * rowBorder: true
 * columnBorder: false
 * +--------------+
 * | Hdr 1  Hdr 2 |
 * |==============|
 * | foo        0 |
 * +--------------+
 * | bar       12 |
 * +--------------+
 * | baz      345 |
 * +--------------+
 *
 * rowBorder: false
 * columnBorder: true
 * +---------------+
 * | Hdr 1 | Hdr 2 |
 * +-------+-------+
 * | foo   |     0 |
 * | bar   |    12 |
 * | baz   |   345 |
 * +---------------+
 *
 * rowBorder: true
 * columnBorder: true
 * +---------------+
 * | Hdr 1 | Hdr 2 |
 * |===============|
 * | foo   |     0 |
 * +-------+-------+
 * | bar   |    12 |
 * +-------+-------+
 * | baz   |   345 |
 * +---------------+
 * ```
 */
const AsciiTableStyle = exports.AsciiTableStyle = {
    columnBorder: ' | ',
    columnNoBorder: '  ',
    paddingChar: ' ',
    leftOutline: '| ',
    rightOutline: ' |',
    topOutlineColumnBorder: '---',
    borderCrossing: '-+-',
    noBorderCrossing: '--',
    bottomOutlineColumnBorder: '---',
    topLeftOutline: '+-',
    bottomLeftOutline: '+-',
    topRightOutline: '-+',
    bottomRightOutline: '-+',
    rowBorder: '-',
    horizontalOutline: '-',
    leftOutlineRowBorder: '+-',
    rightOutlineRowBorder: '-+',
    leftHeaderRowBorder: '|=',
    rightHeaderRowBorder: '=|',
    headerBorder: '=',
    headerBorderCrossing: '===',
    headerNoBorderCrossing: '==',
    outlineNoBorderCrossing: '--',
};
/**
 * Use rounded corners.
 *
 * ```text
 * rowBorder: false
 * columnBorder: false
 * ╭──────────────╮
 * │ Hdr 1  Hdr 2 │
 * ├──────────────┤
 * │ foo        0 │
 * │ bar       12 │
 * │ baz      345 │
 * ╰──────────────╯
 *
 * rowBorder: true
 * columnBorder: false
 * ╭──────────────╮
 * │ Hdr 1  Hdr 2 │
 * ╞══════════════╡
 * │ foo        0 │
 * ├──────────────┤
 * │ bar       12 │
 * ├──────────────┤
 * │ baz      345 │
 * ╰──────────────╯
 *
 * rowBorder: false
 * columnBorder: true
 * ╭───────┬───────╮
 * │ Hdr 1 │ Hdr 2 │
 * ├───────┼───────┤
 * │ foo   │     0 │
 * │ bar   │    12 │
 * │ baz   │   345 │
 * ╰───────┴───────╯
 *
 * rowBorder: true
 * columnBorder: true
 * ╭───────┬───────╮
 * │ Hdr 1 │ Hdr 2 │
 * ╞═══════╪═══════╡
 * │ foo   │     0 │
 * ├───────┼───────┤
 * │ bar   │    12 │
 * ├───────┼───────┤
 * │ baz   │   345 │
 * ╰───────┴───────╯
 * ```
 */
const RoundedTableStyle = exports.RoundedTableStyle = {
    topLeftOutline: '╭─',
    bottomLeftOutline: '╰─',
    topRightOutline: '─╮',
    bottomRightOutline: '─╯',
};
/**
 * When `rowBorders` is `true`, use a fat line between the header and the body instead of a double line.
 *
 * ```text
 * rowBorder: false
 * columnBorder: false
 * ┌──────────────┐
 * │ Hdr 1  Hdr 2 │
 * ├──────────────┤
 * │ foo        0 │
 * │ bar       12 │
 * │ baz      345 │
 * └──────────────┘
 *
 * rowBorder: true
 * columnBorder: false
 * ┌──────────────┐
 * │ Hdr 1  Hdr 2 │
 * ┝━━━━━━━━━━━━━━┥
 * │ foo        0 │
 * ├──────────────┤
 * │ bar       12 │
 * ├──────────────┤
 * │ baz      345 │
 * └──────────────┘
 *
 * rowBorder: false
 * columnBorder: true
 * ┌───────┬───────┐
 * │ Hdr 1 │ Hdr 2 │
 * ├───────┼───────┤
 * │ foo   │     0 │
 * │ bar   │    12 │
 * │ baz   │   345 │
 * └───────┴───────┘
 *
 * rowBorder: true
 * columnBorder: true
 * ┌───────┬───────┐
 * │ Hdr 1 │ Hdr 2 │
 * ┝━━━━━━━┿━━━━━━━┥
 * │ foo   │     0 │
 * ├───────┼───────┤
 * │ bar   │    12 │
 * ├───────┼───────┤
 * │ baz   │   345 │
 * └───────┴───────┘
 * ```
 */
const FatHeaderBorderTableStyle = exports.FatHeaderBorderTableStyle = {
    leftHeaderRowBorder: '┝━',
    rightHeaderRowBorder: '━┥',
    headerBorder: '━',
    headerBorderCrossing: '━┿━',
    headerNoBorderCrossing: '━━',
};
/**
 * Use a double line for the outline around the table.
 *
 * ```text
 * rowBorder: false
 * columnBorder: false
 * ╔══════════════╗
 * ║ Hdr 1  Hdr 2 ║
 * ╟──────────────╢
 * ║ foo        0 ║
 * ║ bar       12 ║
 * ║ baz      345 ║
 * ╚══════════════╝
 *
 * rowBorder: true
 * columnBorder: false
 * ╔══════════════╗
 * ║ Hdr 1  Hdr 2 ║
 * ╠══════════════╣
 * ║ foo        0 ║
 * ╟──────────────╢
 * ║ bar       12 ║
 * ╟──────────────╢
 * ║ baz      345 ║
 * ╚══════════════╝
 *
 * rowBorder: false
 * columnBorder: true
 * ╔═══════╤═══════╗
 * ║ Hdr 1 │ Hdr 2 ║
 * ╟───────┼───────╢
 * ║ foo   │     0 ║
 * ║ bar   │    12 ║
 * ║ baz   │   345 ║
 * ╚═══════╧═══════╝
 *
 * rowBorder: true
 * columnBorder: true
 * ╔═══════╤═══════╗
 * ║ Hdr 1 │ Hdr 2 ║
 * ╠═══════╪═══════╣
 * ║ foo   │     0 ║
 * ╟───────┼───────╢
 * ║ bar   │    12 ║
 * ╟───────┼───────╢
 * ║ baz   │   345 ║
 * ╚═══════╧═══════╝
 * ```
 */
const DoubleOutlineTableStyle = exports.DoubleOutlineTableStyle = {
    leftOutline: '║ ',
    rightOutline: ' ║',
    topOutlineColumnBorder: '═╤═',
    bottomOutlineColumnBorder: '═╧═',
    topLeftOutline: '╔═',
    bottomLeftOutline: '╚═',
    topRightOutline: '═╗',
    bottomRightOutline: '═╝',
    horizontalOutline: '═',
    leftOutlineRowBorder: '╟─',
    rightOutlineRowBorder: '─╢',
    leftHeaderRowBorder: '╠═',
    rightHeaderRowBorder: '═╣',
    outlineNoBorderCrossing: '══',
};
/**
 * Use a fat line for the outline around the table and for the border between the header and body.
 *
 * ```text
 * rowBorder: false
 * columnBorder: false
 * ┏━━━━━━━━━━━━━━┓
 * ┃ Hdr 1  Hdr 2 ┃
 * ┠──────────────┨
 * ┃ foo        0 ┃
 * ┃ bar       12 ┃
 * ┃ baz      345 ┃
 * ┗━━━━━━━━━━━━━━┛
 *
 * rowBorder: true
 * columnBorder: false
 * ┏━━━━━━━━━━━━━━┓
 * ┃ Hdr 1  Hdr 2 ┃
 * ┣━━━━━━━━━━━━━━┫
 * ┃ foo        0 ┃
 * ┠──────────────┨
 * ┃ bar       12 ┃
 * ┠──────────────┨
 * ┃ baz      345 ┃
 * ┗━━━━━━━━━━━━━━┛
 *
 * rowBorder: false
 * columnBorder: true
 * ┏━━━━━━━┯━━━━━━━┓
 * ┃ Hdr 1 │ Hdr 2 ┃
 * ┠───────┼───────┨
 * ┃ foo   │     0 ┃
 * ┃ bar   │    12 ┃
 * ┃ baz   │   345 ┃
 * ┗━━━━━━━┷━━━━━━━┛
 *
 * rowBorder: true
 * columnBorder: true
 * ┏━━━━━━━┯━━━━━━━┓
 * ┃ Hdr 1 │ Hdr 2 ┃
 * ┣━━━━━━━┿━━━━━━━┫
 * ┃ foo   │     0 ┃
 * ┠───────┼───────┨
 * ┃ bar   │    12 ┃
 * ┠───────┼───────┨
 * ┃ baz   │   345 ┃
 * ┗━━━━━━━┷━━━━━━━┛
 * ```
 */
const FatOutlineTableStyle = exports.FatOutlineTableStyle = {
    leftOutline: '┃ ',
    rightOutline: ' ┃',
    topOutlineColumnBorder: '━┯━',
    bottomOutlineColumnBorder: '━┷━',
    topLeftOutline: '┏━',
    bottomLeftOutline: '┗━',
    topRightOutline: '━┓',
    bottomRightOutline: '━┛',
    horizontalOutline: '━',
    leftOutlineRowBorder: '┠─',
    rightOutlineRowBorder: '─┨',
    leftHeaderRowBorder: '┣━',
    rightHeaderRowBorder: '━┫',
    headerBorder: '━',
    headerBorderCrossing: '━┿━',
    headerNoBorderCrossing: '━━',
    outlineNoBorderCrossing: '━━',
};
const DefaultOptions = exports.DefaultOptions = {
    outline: true,
    columnBorders: false,
    rowBorders: false,
    style: DefaultTableStyle,
};
function stringControlReplacer(_, esc, contr) {
    return esc ? CharReplacement[esc] : '\\u' + contr.charCodeAt(0).toString(16).padStart(4, '0');
}
function jsonReplacer(_key, value) {
    return typeof value === 'bigint' || typeof value === 'symbol' ? String(value) : value;
}
/**
 * Format a table into an array of lines.
 *
 * @param rows
 * @param options
 * @returns array of lines
 */
const formatTable = exports.formatTable = function(rows, options = DefaultOptions) {
    const { header, tabWidth, alignment, headerAlignment, formatCell, rowBorders, columnBorders, style, raw } = options;
    let { outline, colors } = options;
    const maxlens = [];
    const processed = [];
    if (alignment && !AlignRegExp.test(alignment)) {
        throw new Error(`illegal alignment: ${alignment}`);
    }
    if (headerAlignment && !AlignRegExp.test(headerAlignment)) {
        throw new Error(`illegal headerAlignment: ${headerAlignment}`);
    }
    outline ??= true;
    const { columnBorder, columnNoBorder, paddingChar, leftOutline, rightOutline, topOutlineColumnBorder, borderCrossing, noBorderCrossing, bottomOutlineColumnBorder, topLeftOutline, bottomLeftOutline, topRightOutline, bottomRightOutline, rowBorder, horizontalOutline, leftOutlineRowBorder, rightOutlineRowBorder, leftHeaderRowBorder, rightHeaderRowBorder, headerBorder, headerBorderCrossing, headerNoBorderCrossing, outlineNoBorderCrossing, } = style ? { ...DefaultTableStyle, ...style } : DefaultTableStyle;
    if (colors === undefined) {
        colors = (typeof navigator !== "undefined" ? navigator.userAgent.includes("Chrome") :
            typeof process !== "undefined" ? !!process.stdout?.isTTY :
                false);
    }
    const colorMap = colors === true ? AnsiColors :
        !colors ? NoColors :
            { ...AnsiColors, ...colors };
    const tabW = tabWidth ?? 4;
    if (tabW < 1 || (tabW | 0) !== tabW) {
        throw new TypeError(`illegal tabWidth: ${tabWidth}`);
    }
    const { boolean: boolColor, escape: escColor, string: strColor, function: funcColor, header: hdrColor, null: nullColor, number: numColor, other: otherColor, reset: resetFormat, symbol: symColor, } = colorMap;
    function jsonColorReplacer(all, str, bool, nul, num, func, other, symbol) {
        if (str)
            return strColor + all.replace(JSEscapeRegExp, esc => escColor + esc + strColor) + resetFormat;
        if (nul)
            return nullColor + all + resetFormat;
        if (func)
            return funcColor + all + resetFormat;
        if (other)
            return otherColor + all + resetFormat;
        if (symbol)
            return symColor + all + resetFormat;
        if (num)
            return numColor + all + resetFormat;
        return boolColor + all + resetFormat;
    }
    function stringControlReplacerWithColor(_, esc, contr) {
        return escColor + (esc ? CharReplacement[esc] : '\\u' + contr.charCodeAt(0).toString(16).padStart(4, '0')) + resetFormat;
    }
    function symbolControlReplacerWithColor(_, esc, contr) {
        return escColor + (esc ? CharReplacement[esc] : '\\u' + contr.charCodeAt(0).toString(16).padStart(4, '0')) + symColor;
    }
    const formatSimple = formatCell ?? String;
    function processRow(row, alignment, defaultAlignment) {
        const processedRow = [];
        for (let cellIndex = 0; cellIndex < row.length; ++cellIndex) {
            const maxlen = maxlens[cellIndex] ??= { prefix: 0, suffix: 0 };
            const cell = row[cellIndex];
            let align = alignment.charAt(cellIndex);
            if (align === ' ') {
                align = '';
            }
            let cellColor = '';
            let strCell;
            let isJson = false;
            switch (typeof cell) {
                case 'number':
                case 'bigint':
                    align ||= '.';
                    cellColor = numColor;
                    strCell = formatSimple(cell);
                    break;
                case 'boolean':
                    align ||= '<';
                    cellColor = boolColor;
                    strCell = formatSimple(cell);
                    break;
                case 'symbol':
                    align ||= defaultAlignment;
                    cellColor = symColor;
                    strCell = formatSimple(cell);
                    break;
                case 'object':
                    if (cell instanceof Date) {
                        align ||= '.';
                        cellColor = numColor;
                        strCell = formatCell ? formatCell(cell) : cell.toISOString();
                    }
                    else {
                        align ||= defaultAlignment;
                        isJson = true;
                        strCell = formatCell ? formatCell(cell) :
                            JSON.stringify(cell, jsonReplacer, paddingChar.repeat(tabW)).
                                replace(SpecialCharRegExp, stringControlReplacer);
                    }
                    break;
                case 'undefined':
                    align ||= defaultAlignment;
                    cellColor = nullColor;
                    strCell = 'undefined';
                    break;
                case 'string':
                    align ||= defaultAlignment;
                    strCell = cell;
                    break;
                case 'function':
                    align ||= defaultAlignment;
                    cellColor = funcColor;
                    strCell = formatCell ? formatCell(cell) :
                        cell.name ? `[Function: ${cell.name}]` : '[Function (anonymous)]';
                    break;
                default:
                    align ||= defaultAlignment;
                    strCell = formatSimple(cell);
                    break;
            }
            const rawLines = strCell.split('\n');
            for (let lineIndex = 0; lineIndex < rawLines.length; ++lineIndex) {
                const line = rawLines[lineIndex];
                let index = line.indexOf('\t');
                if (index >= 0) {
                    let prev = 0;
                    // expecting the JIT to optimize += better than array buffering
                    let newLine = '';
                    for (;;) {
                        newLine += line.slice(prev, index);
                        const padding = tabW - (newLine.length % tabW);
                        newLine += paddingChar.repeat(padding);
                        ++index;
                        if (index > line.length) {
                            break;
                        }
                        prev = index;
                        index = line.indexOf('\t', prev);
                        if (index < 0) {
                            newLine += line.slice(prev);
                            break;
                        }
                    }
                    rawLines[lineIndex] = newLine;
                }
            }
            let maxPrefix = 0;
            let maxSuffix = 0;
            const lines = [];
            for (const line of rawLines) {
                let cleanLine = line.replace(DoubleWidthRegExp, 'XX').replace(IgnoreRegExp, '');
                if (raw) {
                    cleanLine = cleanLine.replace(AnsiEscapeRegExp, '');
                }
                else {
                    cleanLine = cleanLine.replace(ControlRegExp, '\\u####');
                }
                cleanLine = cleanLine.replace(SurrogatePairRegExp, 'x');
                let prefix = cleanLine.length;
                let suffix;
                if (align === '.') {
                    const dotIndex = cleanLine.indexOf('.');
                    if (dotIndex < 0) {
                        suffix = 0;
                    }
                    else {
                        suffix = prefix - dotIndex;
                        prefix = dotIndex;
                        if (suffix > maxSuffix) {
                            maxSuffix = suffix;
                        }
                    }
                }
                else {
                    suffix = 0;
                }
                if (prefix > maxPrefix) {
                    maxPrefix = prefix;
                }
                lines.push({ line, prefix, suffix });
            }
            if (maxPrefix > maxlen.prefix) {
                maxlen.prefix = maxPrefix;
            }
            if (maxSuffix > maxlen.suffix) {
                maxlen.suffix = maxSuffix;
            }
            if (isJson) {
                for (const item of lines) {
                    let { line } = item;
                    if (colors) {
                        line = line.replace(JsonLexRegExp, jsonColorReplacer);
                    }
                    item.line = line + paddingChar.repeat(maxPrefix - item.prefix);
                    item.prefix = maxPrefix;
                }
            }
            else if (typeof cell === 'string') {
                if (!raw) {
                    if (colors) {
                        for (const item of lines) {
                            item.line = item.line.
                                replace(SpecialCharRegExp, stringControlReplacerWithColor);
                        }
                    }
                    else {
                        for (const item of lines) {
                            item.line = item.line.
                                replace(SpecialCharRegExp, stringControlReplacer);
                        }
                    }
                }
            }
            else if (typeof cell === 'symbol') {
                if (!raw) {
                    if (colors) {
                        for (const item of lines) {
                            item.line = item.line.
                                replace(SpecialCharRegExp, symbolControlReplacerWithColor);
                        }
                    }
                    else {
                        for (const item of lines) {
                            item.line = item.line.
                                replace(SpecialCharRegExp, stringControlReplacer);
                        }
                    }
                }
            }
            processedRow.push({
                align,
                lines,
                color: cellColor,
            });
        }
        return processedRow;
    }
    const processedHeader = header ? processRow(header, headerAlignment ?? alignment ?? '', '-') : null;
    for (const row of rows) {
        processed.push(processRow(row, alignment ?? '', '>'));
    }
    const colsep = columnBorders ? columnBorder : columnNoBorder;
    function alignRow(row, defaultAlignment) {
        const grid = [];
        let maxLines = 0;
        for (const cell of row) {
            const lineCount = cell.lines.length;
            if (lineCount > maxLines) {
                maxLines = lineCount;
            }
        }
        if (maxLines === 0) {
            maxLines = 1;
        }
        for (let lineIndex = 0; lineIndex < maxLines; ++lineIndex) {
            grid.push([]);
        }
        for (let cellIndex = 0; cellIndex < row.length; ++cellIndex) {
            const { prefix: maxPrefix, suffix: maxSuffix } = maxlens[cellIndex];
            const maxLength = maxPrefix + maxSuffix;
            const cell = row[cellIndex];
            const { lines, color } = cell;
            const lineslen = lines.length;
            const align = cell.align || defaultAlignment;
            let lineIndex = 0;
            for (; lineIndex < lineslen; ++lineIndex) {
                const item = lines[lineIndex];
                let { line } = item;
                if (color) {
                    line = color + line + resetFormat;
                }
                if (align === '-') {
                    const itemLength = item.prefix + item.suffix;
                    const space = maxLength - itemLength;
                    const before = (space / 2) | 0;
                    const after = space - before;
                    line = paddingChar.repeat(before) + line + paddingChar.repeat(after);
                }
                else if (align === '>') {
                    const itemLength = item.prefix + item.suffix;
                    const padding = paddingChar.repeat(maxLength - itemLength);
                    line += padding;
                }
                else {
                    line = paddingChar.repeat(maxPrefix - item.prefix) + line + paddingChar.repeat(maxSuffix - item.suffix);
                }
                grid[lineIndex][cellIndex] = line;
            }
            if (lineIndex < maxLines) {
                const padding = paddingChar.repeat(maxLength);
                for (; lineIndex < maxLines; ++lineIndex) {
                    grid[lineIndex][cellIndex] = padding;
                }
            }
        }
        const lines = [];
        for (const line of grid) {
            while (line.length < maxlens.length) {
                const { prefix, suffix } = maxlens[line.length];
                line.push(paddingChar.repeat(prefix + suffix));
            }
            const mid = line.join(colsep);
            lines.push(outline ? leftOutline + mid + rightOutline : mid);
        }
        return lines;
    }
    const lines = [];
    const hdrColSep = columnBorders ? topOutlineColumnBorder : outlineNoBorderCrossing;
    const midColSep = columnBorders ? borderCrossing : noBorderCrossing;
    const ftrColSep = columnBorders ? bottomOutlineColumnBorder : outlineNoBorderCrossing;
    if (outline) {
        lines.push(topLeftOutline + maxlens.map(({ prefix, suffix }) => horizontalOutline.repeat(prefix + suffix)).join(hdrColSep) + topRightOutline);
    }
    const sepMid = maxlens.map(({ prefix, suffix }) => rowBorder.repeat(prefix + suffix)).join(midColSep);
    const seperator = outline ? leftOutlineRowBorder + sepMid + rightOutlineRowBorder : sepMid;
    if (processedHeader) {
        if (colors) {
            for (const cell of processedHeader) {
                cell.color += hdrColor;
            }
        }
        for (const line of alignRow(processedHeader, '-')) {
            lines.push(line);
        }
        if (processed.length > 0) {
            if (rowBorders) {
                const hdrCrossSep = columnBorders ? headerBorderCrossing : headerNoBorderCrossing;
                const mid = maxlens.map(({ prefix, suffix }) => headerBorder.repeat(prefix + suffix)).join(hdrCrossSep);
                if (outline) {
                    lines.push(leftHeaderRowBorder + mid + rightHeaderRowBorder);
                }
                else {
                    lines.push(mid);
                }
            }
            else {
                lines.push(seperator);
            }
        }
    }
    let first = true;
    for (const row of processed) {
        if (first) {
            first = false;
        }
        else if (rowBorders) {
            lines.push(seperator);
        }
        for (const line of alignRow(row, '>')) {
            lines.push(line);
        }
    }
    if (outline) {
        lines.push(bottomLeftOutline + maxlens.map(({ prefix, suffix }) => horizontalOutline.repeat(prefix + suffix)).join(ftrColSep) + bottomRightOutline);
    }
    return lines;
}
/**
 * Format and print a table using `console.log()`.
 *
 * @param rows
 * @param options
 */
const printTable = exports.printTable = function(rows, options = DefaultOptions) {
    console.log(formatTable(rows, options).join('\n'));
}
const DefaultOptionsFromObject = exports.DefaultOptionsFromObject = {
    outline: true,
    columnBorders: false,
    rowBorders: false,
    style: DefaultTableStyle,
};
/**
 * Format a list of objects into an array of lines representing a table.
 *
 * @param rows
 * @param options
 * @returns
 */
const formatTableFromObjects = exports.formatTableFromObjects = function(rows, options = DefaultOptionsFromObject) {
    const { header, indexColumn, indexColumnLabel } = options;
    const converted = [];
    let convertedHeader;
    let keys;
    if (header) {
        convertedHeader = [];
        keys = [];
        for (const hdr of header) {
            if (typeof hdr === 'string') {
                convertedHeader.push(hdr);
                keys.push(hdr);
            }
            else {
                const [key, label] = hdr;
                convertedHeader.push(label);
                keys.push(key);
            }
        }
    }
    else {
        const keySet = new Set();
        for (const row of rows) {
            for (const key in row) {
                if (Object.hasOwn(row, key)) {
                    keySet.add(key);
                }
            }
        }
        keys = convertedHeader = Array.from(keySet);
    }
    if (indexColumn) {
        convertedHeader.unshift(indexColumnLabel ?? 'Index');
        for (let index = 0; index < rows.length; ++index) {
            const obj = rows[index];
            const row = [index];
            for (const key of keys) {
                row.push(key in obj ? obj[key] : '');
            }
            converted.push(row);
        }
    }
    else {
        for (const obj of rows) {
            const row = [];
            for (const key of keys) {
                row.push(key in obj ? obj[key] : '');
            }
            converted.push(row);
        }
    }
    return formatTable(converted, { ...options, header: convertedHeader });
}
/**
 * Format and print list of objects as a table using `console.log()`.
 *
 * @param rows
 * @param options
 */
const printTableFromObjects = exports.printTableFromObjects = function(rows, options = DefaultOptionsFromObject) {
    console.log(formatTableFromObjects(rows, options).join('\n'));
}
//# sourceMappingURL=index.js.map