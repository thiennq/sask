# Servers' Army Swiss Knife 
The Swiss Army Sknife Toolkit for Servers

## Install
```bash
npm i -g https://gitlab.com/thiennq/sask
``` 
*You may need sudo in some cases*

## Commands
```
sask ls --help
sask gen --help
sask wait --help
sask count --help
sask yaml --help
```

## Usecase

### Generate nginx load-balancer config file from docker
```
# for preview
sask ls -j containers -s <project>/<service> | sask gen -t examples/templates/default.conf.tmpl --stdin
# in action
sask ls -j containers -s <project>/<service> | sask gen -t examples/templates/default.conf.tmpl --stdin -o output/default.conf
```

### A better version of docker ps
```
sask ls
sask ls -s <project>
sask ls -s <project>/<service>
```

### Count the healthy containers of a service
```
# count containers which has project=<project>, service=<service>
sask count -s <project>/<service>
# count the healthy containers only
sask count -hs <project>/<service>
```

### Render .env file from centeralize yaml
```
sask yaml -i examples/config.yaml | jq {items:.backend} | sask gen --stdin -t examples/templates/env.tmpl -o backend.env
sask yaml -i examples/config.yaml | jq {items:.frontend} | sask gen --stdin -t examples/templates/env.tmpl -o frontend.env
```

## Read more
- https://mozilla.github.io/nunjucks/
- https://github.com/apocas/dockerode
- http://yargs.js.org/