const Docker = require('../classes/docker');
const {printTableFromObjects} = require("../libs/js-print-table");
const ALL_FIELDS = `
    Id, Names, Image, Name, IP, Ports, Created, 
    Project, Service, ServiceNo,
    WorkingDir, State, Status, Healthy
`.split(',').map(x => x.trim());
const DEAULT_FIELDS = ['Id', 'Name', 'Status'];

exports.command = 'ls'
exports.desc = 'list for service instances healthy'
exports.builder = {
    help: {alias: 'h'},
    filter: {
        alias: 'f',
    },
    s: {
        describe: [
            'shorthand for Service filter:',
            '-s A/B => --filter "{Project:\'A\',Service:\'B\'}"',
            '-s A   => --filter "{Project:\'A\'}"'
        ].join('\n'),
    },
    all: {
        alias: 'a',
    },
    props: {
        alias: 'p',
        describe: 'which properties to extract',
    },
    c: {
        describe: 'shorthand for props CHIPS = Created, Health, IP, Ports, Service\neg: -c CIP => -p Created,IP,Ports',
    },
    json: {
        alias: 'j',
        describe: [
            'output format in json. eg:', 
            '    -j            => [ <objects> ]',
            '    -j containers => { "containers": [<objects>] }'
        ].join('\n')
    },

}
exports.handler = function (argv) {
    const {filter, props, all, json, s, c} = argv;
    const docker = new Docker();
    const options = {all: !!all};
    const wrap = typeof json === 'string' ? json : false;
    let fields = [];

    if (props) {
        fields = props.split(',').map(x => x.trim());
        fields.push(...DEAULT_FIELDS);
    }
    if (c) {
        fields = ['Id', 'Name', 'Status'];
        const fieldList = ['IP', 'Ports', 'Created', 'Service', 'Healthy']
        for (let field of fieldList) {
            const firstLetter = field[0];
            if (c.toUpperCase().includes(firstLetter)) {
                fields.push(field);
            }
        }
    }
    if (s) {
        const parts = s.split('/');
        options.filters = {
            Project: parts[0]
        }
        if (parts[1]) {
            options.filters.Service = parts[1];
        }
    }
    if (!s && filter) {
        try {
            options.filters = eval(`tmp=${filter}`);
        } catch (e) {
            console.error(e);
        }
    }

    docker.listContainers(options)
    .then(containers => {
        const format = json ? 'json' : 'table';
        const data = formatForTable(containers, fields, format);
        const output = (json && wrap) ? {[wrap]: data} : data;
        print(output, format);
    })
}

function print(data, format) {
    switch (format) {
        case 'table': 
            return printTableFromObjects(data, {
                // rowBorders: true
            });
        default:
            return console.log(JSON.stringify(data, null, 2));
    }
}

function formatForTable(containers, fields, format) {
    const deletedFields = fields.length ? [] : ['Image', 'WorkingDir', 'Names', 'ServiceNo', 'State'];
    const filteredFields = [...DEAULT_FIELDS];
    if (format === 'json') {
        filteredFields.push('IP', 'Ports');
    }
    filteredFields.push(...fields);
    for (let f of ALL_FIELDS) {
        if (!filteredFields.includes(f)) {
            deletedFields.push(f);
        }
    }

    containers.forEach(rec => {
        rec.Id = (rec.Id || '').substring(0, 12);
        rec.Ports = portFormat(rec.Ports);
        rec.Status = (rec.Status || '').replace(/\(.+\)/g,'').replace(/\s+/g, ' ').trim();
        rec.Created = rec.Created ? new Date(rec.Created * 1000).toISOString().substring(5, 19).replace('T', ' ') : ''
        if (!deletedFields.includes('Service')) {
            rec.Service = `${rec.Project}/${rec.Service}`;
            delete rec.Project;
        }
        
        for (let f of deletedFields) {
            delete rec[f];
        }
    })
    return containers;
}

function portFormat(Ports) {
    if (!Ports) return '';
    return Ports
    .filter(p => p.IP !== '::')
    .map(
        p => p.PublicPort 
            ? `${p.IP || ''}:${p.PublicPort}:${p.PrivatePort}` 
            : `${p.Type}/${p.PrivatePort}`
    ).join('\n')
}