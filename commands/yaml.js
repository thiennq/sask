const Docker = require('../classes/docker');
const {wrapValue, isNumber} = require('../classes/helper');

const nunjucks = require('nunjucks');
const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');
const env = nunjucks.configure({ autoescape: false });
env.addGlobal('wrapValue', wrapValue);
env.addGlobal('isNumber', isNumber);


exports.command = 'yaml'
exports.desc = 'convert yaml file to json'
exports.builder = {
  input: {
    alias: 'i',
    describe: 'Input file (json)'
  },
  stdin: {

  },
  output: {
    alias: 'o',
  }
}
exports.handler = function (argv) {
    const {input, template, output, stdin} = argv;
    if (!input && !stdin) {
      console.log('Need --stdin or -i <file>');
      console.log('Type: ssak gen -h');
      console.log('      for more information')
      return 
    }
    let str;
    if (stdin) {
      str = fs.readFileSync('/dev/stdin').toString();
    } else {
      const inputPath = path.resolve(process.env.PWD, input);
      str = fs.readFileSync(inputPath).toString();
    }

    const data = yaml.load(str);
    const json = nunjucks.renderString(JSON.stringify(data), Object.assign({}, data));

    // Resolve referrences
    const finalData = JSON.parse(json);

    // Stringify final ouptput
    const text = JSON.stringify(finalData, null, 2);

    if (!output) {
        return console.log(text);
    }
    fs.writeFileSync(
        path.resolve(process.env.PWD, output),
        text
    );
}