const Docker = require('../classes/docker');
const nunjucks = require('nunjucks');
const fs = require('fs');
const path = require('path');

exports.command = 'count'
exports.desc = 'count for service instances healthy'
exports.builder = {
    filter: {
        alias: 'f',
    },
    s: {
        describe: [
            'shorthand for Service filter:',
            '-s A/B => --filter "{Project:\'A\',Service:\'B\'}"',
            '-s A   => --filter "{Project:\'A\'}"'
        ].join('\n'),
    },
    healthy: {
        alias: 'h',
        describe: [
            'append {Healthy:true} to filter',
            ' => accept only healthy containers'
        ].join('\n'),
    }
}
exports.handler = function (argv) {
    const {filter, healthy, s} = argv;
    const docker = new Docker();
    const options = {};
    if (s) {
        const parts = s.split('/');
        options.filters = {
            Project: parts[0]
        }
        if (parts[1]) {
            options.filters.Service = parts[1];
        }
    }
    if (!s && filter) {
        try {
            options.filters = eval(`tmp=${filter}`);
        } catch (e) {
            console.error(e);
        }
    }
    if (healthy) {
        options.filters = options.filters || {};
        options.filters.Healthy = true;
    }

    docker.listContainers(options)
    .then(containers => {
        console.log(containers.length + '')    
    })
}
