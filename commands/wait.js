const Docker = require('../classes/docker');
const nunjucks = require('nunjucks');
const fs = require('fs');
const path = require('path');

exports.command = 'wait'
exports.desc = 'wait for service reach N healthy instances (in docker-compose)'
exports.builder = {
  s: {
    demandOption: true,
    describe: [
      'Service filter:',
      '-s A/B => filter "{Project:\'A\',Service:\'B\'}"'
    ].join('\n'),
  }, 
  instance_number: {
    alias: 'n',
    demandOption: true,
  }, 
  interval: {
    alias: 'i',
    default: 10
  }, 
  timeout: {
    alias: 't',
    default: 10
  }
}
exports.handler = function (argv) {
    const {instance_number, interval, timeout, s} = argv;
    const [project, service] = s.split('/');
    if (!project || !service) {
      console.log('Invalid project/service');
      process.exit(1);
    }
    const docker = new Docker();
    
    docker.waitForHealthyServicesReach(project, service, instance_number, interval, timeout, false)
    .then(async () => {
      process.exit(0)
    })
    .catch((e) => {
      process.exit(-1);
    })
}
