const Docker = require('../classes/docker');
const {wrapValue, isNumber} = require('../classes/helper');

const nunjucks = require('nunjucks');
const fs = require('fs');
const path = require('path');
const env = nunjucks.configure({ autoescape: false });
env.addGlobal('wrapValue', wrapValue);
env.addGlobal('isNumber', isNumber);

exports.command = 'gen'
exports.desc = 'generate nginx config file'
exports.builder = {
  input: {
    alias: 'i',
    describe: 'Input file (json)'
  },
  stdin: {

  },
  template: {
    alias: 't',
    describe: 'Path to template file',
    demandOption: true,
  }, 
  output: {
    alias: 'o',
  }
}
exports.handler = function (argv) {
    const {input, template, output, stdin} = argv;
    if (!input && !stdin) {
      console.log('Need --stdin or -i <file>');
      console.log('Type: ssak gen -h');
      console.log('      for more information')
      return 
    }
    let data;
    if (stdin) {
      const str = fs.readFileSync('/dev/stdin').toString();
      data = JSON.parse(str);
    } else {
      const inputPath = path.resolve(process.env.PWD, input);
      const str = fs.readFileSync(inputPath).toString();
      data = JSON.parse(str);
    }
    
    let templatePath = '', inputPath = '';
    try {
        templatePath = path.resolve(process.env.PWD, template);
    } catch (e) {
        throw 'Template not found';
    }
    
    const tpl = fs.readFileSync(templatePath).toString();
    const text = nunjucks.renderString(tpl, data)
    if (!output) {
        return console.log(text);
    }
    fs.writeFileSync(
        path.resolve(process.env.PWD, output),
        text
    );
}
